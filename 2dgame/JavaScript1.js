﻿//F=true→ジャンプ中 F=true→それ以外
var F = true;
//ジャンプ 
var F1 = false;
var num1;
//キーのやつ
//1:ジャンプ　2,0:移動　3:Bダッシュ
var KeyState = new Array();
//マップの大きさ
var map_x = 400;
var map_y = 16;
//スクリーンの座標
var map_coo = {
    x: 0,
    y: 0
}
//マリオのスコア
var states = {
    time: 999,
    timerstart:0,
    timer:function(){
        this.time--;
        if (this.time <= 0) {
            this.game_failed();
        }
    },
    point: 0,
    coin: 0,
    //合計スコア
    score:0,
    //今の画面　0:タイトル画面 1:ゲーム中 2:リザルト画面 
    type: 0,
    //ハイスコア
    a1highscore: 1000,
    a2highscore: 1000,
    goalline:200,
    game_clear: function () {
        document.getElementById("aaaa").innerHTML = "GAME CLEAR!!";
        clearInterval(this.timerstart);
        clearInterval(aaa[0]);
        clearInterval(aaa[1]);
        setTimeout(function () {
            states.type = 2;
            window.scroll(0, 0);
            document.getElementById("titlegamen").style.visibility = "hidden";
            document.getElementById("gamegamen").style.visibility = "hidden";
            document.getElementById("resultgamen").style.visibility = "visible";
            if (mode_cnt == 1) {
                document.getElementById("ult").innerHTML = "初級";
                document.getElementById("result").innerHTML = "timescore:time×5=" + states.time + "×5=" + states.time * 5 + "<br>totalscore:" + states.score + "+" + states.time * 5 + "=" + parseInt(states.score + states.time * 5);
                if (parseInt(states.score + states.time * 5) > states.a1highscore) {
                    document.getElementById("result").innerHTML = document.getElementById("result").innerHTML + "<br />new record!"
                    states.a1highscore = parseInt(states.score + states.time * 5);
                }
            }
            if (mode_cnt == 2) {
                document.getElementById("ult").innerHTML = "上級";
                document.getElementById("result").innerHTML = "timescore:time×10=" + states.time + "×10=" + states.time * 10 + "<br>totalscore:" + states.score + "+" + states.time * 10 + "=" + parseInt(states.score + states.time * 10);
                if (parseInt(states.score + states.time * 10) > states.a2highscore) {
                    document.getElementById("result").innerHTML = document.getElementById("result").innerHTML + "<br />new record!"
                    states.a2highscore = parseInt(states.score + states.time * 10);
                }
            }
        }
        , 3000);
        
    },
    game_failed: function () {
        clearInterval(this.timerstart);
        clearInterval(aaa[0]);
        clearInterval(aaa[1]);
        this.type = 2;
        window.scroll(0, 0);
        document.getElementById("titlegamen").style.visibility = "hidden";
        document.getElementById("gamegamen").style.visibility = "hidden";
        document.getElementById("resultgamen").style.visibility = "visible";
        if (mode_cnt == 1) {
            document.getElementById("ult").innerHTML = "初級";
            document.getElementById("result").innerHTML = "totalscore:" + this.score;
            if (this.score > states.a1highscore) {
                document.getElementById("result").innerHTML = document.getElementById("result").innerHTML + "<br />new record!"
                states.a1highscore = this.score;
            }
        }
        if (mode_cnt == 2) {
            document.getElementById("ult").innerHTML = "上級";
            document.getElementById("result").innerHTML = "totalscore:" + this.score;
            if (this.score > states.a2highscore) {
                document.getElementById("result").innerHTML = document.getElementById("result").innerHTML + "<br />new record!"
                states.a2highscore = this.score;
            }
        }

    }
}

//ジャンプの時に使う
var cnt1 = 0;
//マリオの描画に使う
var cnt2 = 0;
//ジャンプキー長押し対策
var F2 = false;
//
var aaa = [];
/******************************************/
/*描画・操作用*/

//ブロック・コイン
var myobject = function (y, x, type) {
    //ブロックの種類
    this.type = type;
    //16pxで1マス
    this.x = x;
    //16pxで1マス
    this.y = y;
    //マリオが触れるかどうか
    //触れるならtrue
    this.touch = false;
    //マリオに叩かれたかどうかの判定
    this.hit = function () {
        var Flag = this.touch;
        if (this.type == 3) {
            this.type = 0;
            this.initialize();
            states.point += 100;
        }
        else if (this.type == 4) {
            this.type = 5;
            this.initialize();
            states.coin += 1;
            states.point += 200;
        }
        else if (this.type == 6) {
            this.type = 5;
            this.initialize();
            states.coin += 1;
            states.point += 300;
            Flag = true;
        }
        return Flag;
    }
    //コインのためだけの関数
    this.getcoin = function () {
        if (this.type == 7) {
            this.type = 0;
            this.initialize();
            states.coin += 1;
            states.point+= 100;
        }
    }
    this.img = new Array();
    for (var i = 0; i < 16; i++) {
        this.img = new Array();
    }
    this.initialize = function () {
        if (this.type == 0) {
            //えあ
            for (var i = 0; i < 16; i++) {
                this.img[i] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            }
            this.touch = false;
        }
        else if (this.type == 1) {
            //地面 壊せない
            this.img[00] = [2, 3, 3, 3, 3, 3, 3, 3, 3, 1, 2, 3, 3, 3, 3, 2];
            this.img[01] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[02] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[03] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[04] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 1, 2, 2, 2, 1];
            this.img[05] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1, 2];
            this.img[06] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 3, 3, 3, 3, 1];
            this.img[07] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[08] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[09] = [3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 1];
            this.img[10] = [1, 1, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 1];
            this.img[11] = [3, 3, 1, 1, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 1];
            this.img[12] = [3, 2, 3, 3, 1, 1, 1, 1, 3, 2, 2, 2, 2, 2, 2, 1];
            this.img[13] = [3, 2, 2, 2, 3, 3, 3, 1, 3, 2, 2, 2, 2, 2, 2, 1];
            this.img[14] = [3, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 1, 1];
            this.img[15] = [2, 1, 1, 1, 1, 1, 1, 2, 3, 1, 1, 1, 1, 1, 1, 2];
            this.touch = true;
        }
        else if (this.type == 2) {
            //壊せないブロック
            this.img[00] = [2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1];
            this.img[01] = [3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1];
            this.img[02] = [3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1];
            this.img[03] = [3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1];
            this.img[04] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[05] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[06] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[07] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[08] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[09] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[10] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[11] = [3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1];
            this.img[12] = [3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1];
            this.img[13] = [3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1];
            this.img[14] = [3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1];
            this.img[15] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2];
            this.touch = true;
        }
        else if (this.type == 3) {
            //普通の、スーパーマリオ状態で破壊できるブロック
            this.img[00] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3];
            this.img[01] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[02] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[03] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            this.img[04] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[05] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[06] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[07] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            this.img[08] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[09] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[10] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[11] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            this.img[12] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[13] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[14] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            this.img[15] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            this.touch = true;
        }
        else if (this.type == 4) {
            //？ブロック
            this.img[00] = [0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0];
            this.img[01] = [2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1];
            this.img[02] = [2, 4, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 4, 1];
            this.img[03] = [2, 4, 4, 4, 4, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 1];
            this.img[04] = [2, 4, 4, 4, 2, 2, 1, 1, 1, 2, 2, 4, 4, 4, 4, 1];
            this.img[05] = [2, 4, 4, 4, 2, 2, 1, 4, 4, 2, 2, 1, 4, 4, 4, 1];
            this.img[06] = [2, 4, 4, 4, 2, 2, 1, 4, 4, 2, 2, 1, 4, 4, 4, 1];
            this.img[07] = [2, 4, 4, 4, 4, 1, 1, 4, 2, 2, 2, 1, 4, 4, 4, 1];
            this.img[08] = [2, 4, 4, 4, 4, 4, 4, 2, 2, 1, 1, 1, 4, 4, 4, 1];
            this.img[09] = [2, 4, 4, 4, 4, 4, 4, 2, 2, 1, 4, 4, 4, 4, 4, 1];
            this.img[10] = [2, 4, 4, 4, 4, 4, 4, 4, 1, 1, 4, 4, 4, 4, 4, 1];
            this.img[11] = [2, 4, 4, 4, 4, 4, 4, 2, 2, 4, 4, 4, 4, 4, 4, 1];
            this.img[12] = [2, 4, 4, 4, 4, 4, 4, 2, 2, 1, 4, 4, 4, 4, 4, 1];
            this.img[13] = [2, 4, 1, 4, 4, 4, 4, 4, 1, 1, 4, 4, 4, 1, 4, 1];
            this.img[14] = [2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1];
            this.img[15] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            this.touch = true;
        }
        else if (this.type == 5) {
            this.img[00] = [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
            this.img[01] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[02] = [1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1];
            this.img[03] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[04] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[05] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[06] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[07] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[08] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[09] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[10] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[11] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[12] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[13] = [1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1];
            this.img[14] = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1];
            this.img[15] = [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
            this.touch = true;
        }
        else if (this.type == 6) {
            //隠しブロック
            for (var i = 0; i < 16; i++) {
                this.img[i] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            }
            this.touch = false;
        }
        else if (this.type == 7) {
            //コイン
            this.img[00] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            this.img[01] = [0, 0, 0, 0, 0, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0, 0];
            this.img[02] = [0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0];
            this.img[03] = [0, 0, 0, 0, 4, 4, 2, 2, 4, 4, 1, 1, 0, 0, 0, 0];
            this.img[04] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[05] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[06] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[07] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[08] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[09] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[10] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[11] = [0, 0, 0, 4, 4, 2, 4, 4, 1, 4, 4, 1, 1, 0, 0, 0];
            this.img[12] = [0, 0, 0, 0, 4, 4, 1, 1, 4, 4, 1, 1, 0, 0, 0, 0];
            this.img[13] = [0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0];
            this.img[14] = [0, 0, 0, 0, 0, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0, 0];
            this.img[15] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            this.touch = false;
        }
        else if (this.type == 8) {
            //ゴール
            for (var i = 0; i < 16; i++) {
                this.img[i] = [9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9];
            }
            this.touch = false;
        }
        for (var i = 0; i < 16; i++) {
            for (var j = 0; j < 16; j++) {
                this.img[i][j] = SCD(this.img[i][j]);
                if (this.touch) {
                    stgmake[1][this.y * 16 + i][this.x * 16 + j] = 1;
                } else {
                    stgmake[1][this.y * 16 + i][this.x * 16 + j] = 0;
                }
                stgmake[0][this.y * 16 + i][this.x * 16 + j] = this.img[i][j];
            }
        }
        for (var i = 0; i < 16; i++) {
            for (var j = 0; j < 16; j++) {
                ctx.fillStyle = stgmake[0][this.y * 16 + i][this.x * 16 + j];
                ctx.fillRect(num1 * (this.x * 16 + j), num1 * (this.y * 16 + i), num1, num1);
            }
        }

    }
}

var MB = new Array();
for (var i = 0; i < map_y; i++) {
    MB[i] = new Array();
}


var creature = function () {
    //大きさ
    this.sizex = 16;
    this.sizey = 16;
    //X座標
    this.x = [16];
    //Y座標
    this.y = [220];
    //当たり判定。何でもできる。
    this.cdx = [4, 15];
    this.cdy=[2,14]
    //X座標(描画用とか)
    this.bx = [parseInt(this.x), 0];
    //Y座標(描画用とか)
    this.by = [parseInt(this.y), 0];
    //向いている方向
    this.direction = ["right", 0];
    //うえに障害物あり：true なし：false
    this.above = false;
    //したに障害物あり：true なし：false
    this.bottom = false;
    //みぎに障害物あり：true なし：false
    this.right = false;
    //ひだりに障害物あり：true なし：false
    this.left = false;
    //なんかの倍率。数値が大きい程ふんわりした動きになる
    this.scalex = 40;
    this.scaley = 20;
    //スピードとか
    this.disx = parseInt(this.x * this.scalex);
    this.disy = parseInt(this.y * this.scaley);
    this.speedx = 0;
    this.speedy = 0;
    //マリオがBダッシュしたときの最大速度
    this.maxspeed = parseInt(1.5*this.scalex);
    //マリオの画像
    this.img = new Array();
    this.byoga = function () {
        /* 0=停止中 1~3=移動中 4=ジャンプ中 5=滑ってる時 */
        for (var i = 0; i < 99; i++) {
            this.img[i] = new Array();
            for (var j = 0; j < 50; j++) {
                this.img[i][j] = new Array();
            }
        }
        this.img[0][00] = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0];
        this.img[0][01] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
        this.img[0][02] = [0, 0, 0, 0, 0, 0, 3, 3, 3, 2, 2, 3, 2, 0, 0, 0];
        this.img[0][03] = [0, 0, 0, 0, 0, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2, 0];
        this.img[0][04] = [0, 0, 0, 0, 0, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2, 2];
        this.img[0][05] = [0, 0, 0, 0, 0, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 0];
        this.img[0][06] = [0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0];
        this.img[0][07] = [0, 0, 0, 0, 0, 0, 3, 3, 1, 3, 3, 3, 0, 0, 0, 0];
        this.img[0][08] = [0, 0, 0, 0, 0, 3, 3, 3, 1, 3, 3, 1, 3, 3, 3, 0];
        this.img[0][09] = [0, 0, 0, 0, 3, 3, 3, 3, 1, 1, 1, 1, 3, 3, 3, 3];
        this.img[0][10] = [0, 0, 0, 0, 2, 2, 3, 1, 2, 1, 1, 2, 1, 3, 2, 2];
        this.img[0][11] = [0, 0, 0, 0, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2];
        this.img[0][12] = [0, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2];
        this.img[0][13] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0];
        this.img[0][14] = [0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 3, 3, 3, 0];
        this.img[0][15] = [0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 3, 3, 3, 3];

        this.img[1][00] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0];
        this.img[1][01] = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0];
        this.img[1][02] = [0, 0, 0, 0, 0, 3, 3, 3, 2, 2, 3, 2, 0, 0, 0, 0];
        this.img[1][03] = [0, 0, 0, 0, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2, 0, 0];
        this.img[1][04] = [0, 0, 0, 0, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2, 2, 0];
        this.img[1][05] = [0, 0, 0, 0, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 0, 0];
        this.img[1][06] = [0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0];
        this.img[1][07] = [0, 0, 0, 3, 3, 3, 3, 1, 1, 3, 3, 0, 0, 0, 0, 0];
        this.img[1][08] = [0, 2, 2, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 2, 2, 2];
        this.img[1][09] = [0, 2, 2, 2, 0, 3, 3, 1, 2, 1, 1, 1, 3, 3, 2, 2];
        this.img[1][10] = [0, 2, 2, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 3, 0];
        this.img[1][11] = [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 0];
        this.img[1][12] = [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 0];
        this.img[1][13] = [0, 0, 3, 3, 1, 1, 1, 0, 0, 0, 1, 1, 1, 3, 3, 0];
        this.img[1][14] = [0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.img[1][15] = [0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        this.img[2][00] = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0];
        this.img[2][01] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
        this.img[2][02] = [0, 0, 0, 0, 0, 0, 3, 3, 3, 2, 2, 3, 2, 0, 0, 0];
        this.img[2][03] = [0, 0, 0, 0, 0, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2, 0];
        this.img[2][04] = [0, 0, 0, 0, 0, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2, 2];
        this.img[2][05] = [0, 0, 0, 0, 0, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 0];
        this.img[2][06] = [0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0];
        this.img[2][07] = [0, 0, 0, 0, 0, 0, 3, 3, 1, 3, 3, 3, 0, 0, 0, 0];
        this.img[2][08] = [0, 0, 0, 0, 0, 3, 3, 3, 3, 1, 1, 3, 3, 0, 0, 0];
        this.img[2][09] = [0, 0, 0, 0, 0, 3, 3, 3, 1, 1, 2, 1, 1, 2, 0, 0];
        this.img[2][10] = [0, 0, 0, 0, 0, 3, 3, 3, 3, 1, 1, 1, 1, 1, 0, 0];
        this.img[2][11] = [0, 0, 0, 0, 0, 1, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0];
        this.img[2][12] = [0, 0, 0, 0, 0, 0, 1, 3, 2, 2, 1, 1, 1, 0, 0, 0];
        this.img[2][13] = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 3, 3, 0, 0, 0];
        this.img[2][14] = [0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 0, 0];
        this.img[2][15] = [0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 0];

        this.img[3][00] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.img[3][01] = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0];
        this.img[3][02] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
        this.img[3][03] = [0, 0, 0, 0, 0, 0, 3, 3, 3, 2, 2, 3, 2, 0, 0, 0];
        this.img[3][04] = [0, 0, 0, 0, 0, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2, 0];
        this.img[3][05] = [0, 0, 0, 0, 0, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2, 2];
        this.img[3][06] = [0, 0, 0, 0, 0, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 0];
        this.img[3][07] = [0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0];
        this.img[3][08] = [0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 1, 3, 0, 2, 0, 0];
        this.img[3][09] = [0, 0, 0, 0, 0, 2, 3, 3, 3, 3, 3, 3, 2, 2, 2, 0];
        this.img[3][10] = [0, 0, 0, 0, 2, 2, 1, 3, 3, 3, 3, 3, 2, 2, 0, 0];
        this.img[3][11] = [0, 0, 0, 0, 3, 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0];
        this.img[3][12] = [0, 0, 0, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0];
        this.img[3][13] = [0, 0, 0, 3, 3, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0];
        this.img[3][14] = [0, 0, 0, 3, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0];
        this.img[3][15] = [0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0];

        this.img[4][00] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2];
        this.img[4][01] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 2, 2, 2];
        this.img[4][02] = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2];
        this.img[4][03] = [0, 0, 0, 0, 0, 3, 3, 3, 2, 2, 3, 2, 0, 3, 3, 3];
        this.img[4][04] = [0, 0, 0, 0, 3, 2, 3, 2, 2, 2, 3, 2, 2, 3, 3, 3];
        this.img[4][05] = [0, 0, 0, 0, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2, 2, 3];
        this.img[4][06] = [0, 0, 0, 0, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 3, 0];
        this.img[4][07] = [0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 3, 0, 0];
        this.img[4][08] = [0, 0, 3, 3, 3, 3, 3, 1, 3, 3, 3, 1, 3, 0, 0, 0];
        this.img[4][09] = [0, 3, 3, 3, 3, 3, 3, 3, 1, 3, 3, 3, 1, 0, 0, 3];
        this.img[4][10] = [2, 2, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 0, 0, 3];
        this.img[4][11] = [2, 2, 2, 0, 1, 1, 3, 1, 1, 2, 1, 1, 2, 1, 3, 3];
        this.img[4][12] = [0, 2, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3];
        this.img[4][13] = [0, 0, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3];
        this.img[4][14] = [0, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0];
        this.img[4][15] = [0, 3, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0];

        this.img[5][00] = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0];
        this.img[5][01] = [0, 0, 0, 0, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0];
        this.img[5][02] = [0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 2, 3, 2, 0, 0, 0];
        this.img[5][03] = [0, 0, 0, 2, 2, 3, 2, 2, 3, 2, 2, 2, 2, 2, 2, 0];
        this.img[5][04] = [0, 0, 0, 2, 2, 3, 2, 2, 3, 3, 2, 2, 3, 3, 2, 2];
        this.img[5][05] = [0, 0, 0, 0, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3, 3, 0];
        this.img[5][06] = [0, 0, 0, 0, 0, 1, 1, 1, 3, 3, 3, 1, 2, 2, 0, 0];
        this.img[5][07] = [0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 1, 1, 3, 3, 3, 0];
        this.img[5][08] = [0, 0, 0, 0, 1, 3, 2, 2, 2, 3, 3, 3, 3, 3, 3, 0];
        this.img[5][09] = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 0];
        this.img[5][10] = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 3, 3, 3, 3, 0, 0];
        this.img[5][11] = [0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 1, 1, 1, 0, 0, 0];
        this.img[5][12] = [0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 1, 1, 1, 0, 0, 0];
        this.img[5][13] = [0, 0, 0, 3, 0, 3, 1, 1, 3, 3, 3, 1, 0, 0, 0, 0];
        this.img[5][14] = [0, 0, 0, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, 0, 0, 0];
        this.img[5][15] = [0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < 6; i++) {
            for (var j = 0; j < 16; j++) {
                for (var k = 0; k < 16; k++) {
                    this.img[i][j][k] = MC(this.img[i][j][k]);
                }
            }
        }
    }
    //マリオの絵のタイプ
    this.drowtype = new Array(0, 0);
    this.collision_detection = function () {

        if ((stgmake[1][this.y[0] + this.cdy[0] - 1][this.x[0] + mario.cdx[0]] != 0) || (stgmake[1][this.y[0] - this.cdy[0] - 1][this.x[0] + mario.cdx[1]] != 0)) {
            //上に障害物がある場合
            this.above = true;
        }
        else { this.above = false; }
        if ((stgmake[1][this.y[0] + this.cdy[1] + 1][this.x[0] + mario.cdx[0]] != 0) || (stgmake[1][this.y[0] + this.cdy[1] + 1][this.x[0] + mario.cdx[1]] != 0)) {
            //下に障害物がある場合
            this.bottom = true;
        }
        else { this.bottom = false; }
        if ((stgmake[1][this.y[0] + this.cdy[0]][this.x[0] + mario.cdx[1] + 1] != 0) || (stgmake[1][this.y[0] + this.cdy[1]][this.x[0] + mario.cdx[1] + 1] != 0)) {
            //右に障害物がある場合
            this.right = true;
        }
        else { this.right = false; }
        if ((stgmake[1][this.y[0] + this.cdy[0]][this.x[0] + mario.cdx[0] - 1] != 0) || (stgmake[1][this.y[0] + this.cdy[1]][this.x[0] + mario.cdx[0] - 1] != 0)) {
            //左に障害物がある場合
            this.left = true;
        }
        else { this.left = false; }
    }
    this.change = function () {
        this.sizex = 16;
        this.sizey = 32;
    }
}
var mario = new creature();

/******************************************/
/*ステージ描画用　名前[縦(y)][横(x)]*/
var canvas, ctx
var sta = new Array();

var poi = new Array();
for (var i = 0; i < 16; i++) {
    poi[i] = new Array();
}
var stgmake = new Array();

/******************************************/
/************************************************************/
// FPS計算 ---------------------------------------------ここら辺よくわからなi
var FPS = function (target) {
    this.target = target;        // 目標FPS
    this.interval = 1000 / target; // setTimeoutに与えるインターバル
    this.checkpoint = new Date();
    this.fps = 0;
};
FPS.prototype = {
    // checkからcheckまでの時間を元にFPSを計算
    check: function () {
        var now = new Date();
        this.fps = 1000 / (now - this.checkpoint);
        this.checkpoint = new Date();
    },
    // 現在のFPSを取得
    getFPS: function () {
        return this.fps.toFixed(2);
    },
    // 次回処理までのインターバルを取得
    getInterval: function () {
        var elapsed = new Date() - this.checkpoint;
        return this.interval - elapsed > 10 ? this.interval - elapsed : 10;
    }
};
var fps = new FPS(60);
/******************************************/
/******************************************/
document.onkeydown = mario_movestart;
document.onkeyup = mario_movestop;
window.onload = page_onload;
/******************************************/
function page_onload() {//多分一番最初に一回だけ行われる

    map_coo.x = 0;
    window.scroll(0, 0);
    canvas = document.getElementById("red_box");
    ctx = canvas.getContext("2d");
    num1 = 2;
    canvas.style.backgroundColor = "#ffffff";


    document.getElementById("titlegamen").style.visibility =  "visible";
    document.getElementById("gamegamen").style.visibility = "hidden";
    document.getElementById("resultgamen").style.visibility = "hidden";
    document.getElementById("aaaa").innerHTML = "";
    document.getElementById("level").style.visibility = "hidden";
    document.getElementById("setumei").style.visibility = "visible";
    document.getElementById("button").innerHTML = "Aボタンを押してください";
    document.getElementById("highscore").innerHTML = "初級 highscore:" + states.a1highscore + "<br>上級 highscore:" + states.a2highscore;
}
/******************************************/
/*マリオ操作*******************************/
/******************************************/
var mode = 0;
var mode_cnt = 2;
function mario_movestart() {
    switch (event.keyCode) {
        case 16://Shift
            KeyState[3] = true;
            break;
        case 37://←
            KeyState[0] = true;
            KeyState[2] = false;
            break;
        case 38://↑
            if (mode == 1 && states.type == 0) {
                if (mode_cnt != 1) {
                    document.getElementById("a" + mode_cnt).style.backgroundColor = "#000000";
                    mode_cnt--;
                    document.getElementById("a" + mode_cnt).style.backgroundColor = "#ffff00";
                }
            }
            break;
        case 39://→
            KeyState[2] = true;
            KeyState[0] = false;
            break;
        case 40://↓
            if (mode == 1&&states.type == 0) {
                if (mode_cnt != 2) {
                    document.getElementById("a" + mode_cnt).style.backgroundColor = "#000000";
                    mode_cnt++;
                    document.getElementById("a" + mode_cnt).style.backgroundColor = "#ffff00";
                }
                }
                break;
        case 90://z
            if (states.type == 0) {
                if (mode == 0) {
                    document.getElementById("setumei").style.visibility = "hidden";
                    document.getElementById("level").style.visibility = "visible";
                    document.getElementById("a" + mode_cnt).style.backgroundColor = "#ffff00";
                    document.getElementById("button").innerHTML = "スティックで難易度選択";
                    mode = 1;
                } else if (mode == 1) {

                    document.getElementById("button").innerHTML = "ロード中";
                    setTimeout(function () {
                        if (mode_cnt == 1) {
                            mario.scalex = 30;
                            mario.scaley = 20;
                            mario.disx = parseInt(mario.x * mario.scalex);
                            mario.disy = parseInt(mario.y * mario.scaley);
                            mario.maxspeed = parseInt(1.5 * mario.scalex);
                            states.time = 800;
                            map_x = 160;
                            map_y = 16;
                            states.goalline = 150;
                            for (var i = 0; i < 2; i++) {
                                stgmake[i] = new Array();
                                for (var j = -100 * map_y; j < 16 * (map_y + 3) ; j++) {
                                    stgmake[i][j] = new Array();
                                    for (var k = 0; k < 16 * map_x; k++) {
                                        stgmake[i][j][k] = 0;
                                    }
                                }
                            }
                            canvas.width = num1 * map_x * 16;
                            canvas.height = num1 * map_y * 16;
                            sta[00] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[01] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 7, 7, 7, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[02] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 7, 7, 0, 0, 7, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 7, 7, 7, 7, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 7, 7, 7, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[03] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 2, 7, 7, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[04] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 2, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 0, 0, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[05] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 7, 7, 7, 0, 3, 3, 3, 0, 0, 6, 4, 4, 4, 4, 4, 4, 4, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[06] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 4, 2, 4, 4, 4, 4, 2, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[07] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 3, 3, 3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 0, 2, 7, 7, 7, 3, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 3, 3, 3, 0, 7, 7, 7, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[08] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[09] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 7, 7, 0, 0, 0, 0, 3, 3, 3, 7, 7, 7, 3, 3, 3, 7, 7, 7, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[10] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 7, 3, 7, 3, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[11] = [0, 0, 0, 0, 7, 7, 7, 0, 3, 3, 3, 3, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 2, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4, 4, 4, 4, 4, 4, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 4, 0, 0, 0, 4, 0, 0, 7, 7, 7, 2, 4, 4, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 7, 7, 7, 2, 2, 2, 2, 6, 2, 3, 2, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[12] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[13] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[14] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[15] = [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];


                        } if (mode_cnt == 2) {
                            mario.scalex = 40;
                            mario.scaley = 20;
                            mario.disx = parseInt(mario.x * mario.scalex);
                            mario.disy = parseInt(mario.y * mario.scaley);
                            mario.maxspeed = parseInt(1.5 * mario.scalex);
                            states.time = 500;
                            map_x = 210;
                            map_y = 16;
                            states.goalline = 200;
                            for (var i = 0; i < 2; i++) {
                                stgmake[i] = new Array();
                                for (var j = -100 * map_y; j < 16 * (map_y + 3) ; j++) {
                                    stgmake[i][j] = new Array();
                                    for (var k = 0; k < 16 * map_x; k++) {
                                        stgmake[i][j][k] = 0;
                                    }
                                }
                            }
                            canvas.width = num1 * map_x * 16;
                            canvas.height = num1 * map_y * 16;
                            sta[00] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[01] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[02] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[03] = [0, 0, 7, 6, 7, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[04] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 7, 7, 7, 7, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[05] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[06] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[07] = [0, 0, 3, 4, 3, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 3, 0, 3, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 2, 2, 2, 2, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[08] = [0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[09] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[10] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 3, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 6, 6, 6, 6, 6, 2, 2, 2, 0, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 2, 0, 2, 7, 2, 3, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[11] = [0, 3, 4, 3, 4, 3, 0, 0, 0, 3, 0, 3, 0, 0, 0, 7, 0, 0, 0, 7, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 7, 2, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 2, 0, 0, 0, 3, 0, 6, 6, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 7, 7, 7, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 7, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[12] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 7, 7, 7, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[13] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 7, 7, 7, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[14] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 7, 7, 7, 0, 0, 0, 0, 7, 7, 7, 7, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 2, 0, 7, 7, 7, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                            sta[15] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

                        }
                        for (var i = 0; i < map_y; i++) {
                            for (var j = 0; j < map_x; j++) {
                                MB[i][j] = new myobject(i, j, sta[i][j]);
                                MB[i][j].initialize();
                            }
                        }
                        mario.byoga();
                        mario_drow();
                        document.getElementById("titlegamen").style.visibility = "hidden";
                        document.getElementById("gamegamen").style.visibility = "visible";
                        document.getElementById("resultgamen").style.visibility = "hidden";
                        states.type = 1;

                        aaa[0] = setInterval("mario_drow()", fps.getInterval());
                        aaa[1] = setInterval("mario_move()", 8);
                        states.timerstart = setInterval("states.timer()", 500);
                    }, 0);
                }
            }
            else if (states.type == 2) {
                states.type = 0;
                states.coin = 0;
                states.point = 0;
                mode = 0;
                mario = new creature();
                page_onload();
            }
            else {
                KeyState[1] = true;
            }
            break;
        case 13:
            page_onload();
            return false;
            break;
    }
}
/******************************************/
function mario_movestop() {
    switch (event.keyCode) {
        case 16://Shift
            KeyState[3] = false;
            break;
        case 37://←
            KeyState[0] = false;
            break;
        case 39://→
            KeyState[2] = false;
            break;
        case 90://z
            KeyState[1] = false;
            F2 = false;
            break;
    }
}
/******************************************/
function mario_move() {

    mario.collision_detection();

    if (mario.bottom) {
        mario.speedy = 0;
        mario.disy = mario.y[0] * mario.scaley;
        F = false;
        F1 = false;
        if (KeyState[1] && !F2) {
            F = true;
            F2 = true;
            mario.speedy = -Math.sqrt(2 * (64 + Math.abs(mario.speedx) / mario.maxspeed * 16) * mario.scaley);
        }
    }
    else {
        if (F && !KeyState[1] && mario.speedy <= -Math.sqrt(2 * 16 * mario.scaley)) {
            mario.speedy = -Math.sqrt(20 * mario.scaley);
            F = false;
        }
        if (mario.speedy >= Math.sqrt(2 * 80 * mario.scaley)) {
            mario.speedy = Math.sqrt(2 * 80 * mario.scaley);
        } else {
            mario.speedy++;
        }
    }

    /*横移動******************************/
    if (!mario.bottom) {
        if (KeyState[0] && !mario.left) {
            if (KeyState[3]) {
                if (mario.speedx > -mario.maxspeed) {
                    mario.speedx -= 1;
                }
                if (mario.speedx < -mario.maxspeed) {
                    mario.speedx = -mario.maxspeed;
                }
            }
            else {
                if (mario.speedx > -mario.scalex) {
                    mario.speedx -= 1;
                }
                else if (mario.speedx < -mario.scalex) {
                    mario.speedx += 1;
                }
            }
            mario.direction[0] = "left";
        }
        else if (KeyState[2] && !mario.right) {
            if (KeyState[3]) {
                if (mario.speedx < mario.maxspeed) {
                    mario.speedx += 1;
                }
                if (mario.speedx > mario.maxspeed) {
                    mario.speedx = mario.maxspeed;
                }
            }
            else {
                if (mario.speedx < mario.scalex) {
                    mario.speedx += 1;
                }
                else if (mario.speedx > mario.scalex) {
                    mario.speedx -= 1;
                }
            }
            mario.direction[0] = "right";
            
        }
    }
    else {
        if (KeyState[0] && !mario.left) {
            if (KeyState[3]) {
                if (mario.speedx > -mario.maxspeed) {
                    mario.speedx -= 2;
                }
                if (mario.speedx < -mario.maxspeed) {
                    mario.speedx = -mario.maxspeed;
                }
            }
            else {
                if (mario.speedx > -mario.scalex) {
                    mario.speedx -= 2;
                }
                else if (mario.speedx < -mario.scalex) {
                    mario.speedx += 2;
                }
            }
            mario.direction[0] = "left";
        }
        else if (KeyState[2] && !mario.right) {
            if (KeyState[3]) {
                if (mario.speedx < mario.maxspeed) {
                    mario.speedx += 2;
                }
                if (mario.speedx > mario.maxspeed) {
                    mario.speedx = mario.maxspeed;
                }
            }
            else {
                if (mario.speedx < mario.scalex) {
                    mario.speedx += 2;
                }
                else if (mario.speedx > mario.scalex) {
                    mario.speedx -= 2;
                }
            }
            mario.direction[0] = "right";

        } else {
            if (Math.abs(mario.speedx) < Math.abs(2)) {
                mario.speedx = 0;
            }
            else if (mario.speedx < 0) {
                mario.speedx += 2;
            }
            else if (mario.speedx > 0) {
                mario.speedx -= 2;
            }
        }
    }
    if (mario.speedx == 0) {
        mario.disx = mario.x[0] * mario.scalex;
    }
    mario.disx += mario.speedx;
    mario.x[0] = Math.floor(mario.disx / mario.scalex);

    if (stgmake[1][mario.y[0] + mario.cdy[0]][mario.x[0] + mario.cdx[0]] != 0 || stgmake[1][mario.y[0] + mario.cdy[1]][mario.x[0] + mario.cdx[0]] != 0) {
        //左側がめり込んでいる場合
            mario.x[0] = mario.x[0] + 1;
            if (mario.speedx <= 0) {
                mario.speedx = 0;
            }
    }
    if (stgmake[1][mario.y[0] + mario.cdy[0]][mario.x[0] + mario.cdx[1]] != 0 || stgmake[1][mario.y[0] + mario.cdy[1]][mario.x[0] + mario.cdx[1]] != 0) {
        //右側がめり込んでいる場合
            mario.x[0] = mario.x[0] - 1;
            if (mario.speedx >= 0) {
                mario.speedx = 0;
            }
    }
    /*************************************/

    /*縦移動******************************/
    mario.disy += mario.speedy;
    mario.y[0] = Math.floor(mario.disy / mario.scaley);
    if (mario.y[0] >= map_y * 16) {
        /*死んだー*/
        clearInterval(states.timerstart);
        clearInterval(aaa[0]);
        clearInterval(aaa[1]);
        states.game_failed();
    }
    if (!F1 && mario.speedy < 0 && mario.y[0] > 0 && MB[Math.floor((mario.y[0] + mario.cdy[0]) / 16)][Math.floor((mario.x[0] + 10) / 16)].hit()) {
        //上にめり込んだ場合
            mario.y[0] = Math.ceil(mario.y[0] / 16) * 16 - mario.cdy[0];
            mario.speedy = -mario.speedy;
            F1 = true;
    }
    if ((stgmake[1][mario.y[0] + mario.cdy[1]][mario.x[0] + mario.cdx[0]] != 0) || (stgmake[1][mario.y[0] + mario.cdy[1]][mario.x[0] + mario.cdx[1]] != 0)) {
        //下にめり込んだ場合
            mario.y[0] = Math.floor(mario.y[0] / 16) * 16 + 1;
            mario.speedy = 0;
    }
    /*************************************/
    /*コイン******************************/
    if (mario.y > 0) {
        MB[Math.floor((mario.y[0] + mario.cdy[0]) / 16)][Math.floor((mario.x[0] + mario.cdx[0]) / 16)].getcoin();
        MB[Math.floor((mario.y[0] + mario.cdy[1]) / 16)][Math.floor((mario.x[0] + mario.cdx[0]) / 16)].getcoin();
        MB[Math.floor((mario.y[0] + mario.cdy[0]) / 16)][Math.floor((mario.x[0] + mario.cdx[1]) / 16)].getcoin();
        MB[Math.floor((mario.y[0] + mario.cdy[1]) / 16)][Math.floor((mario.x[0] + mario.cdx[1]) / 16)].getcoin();
    }
    /*************************************/


    mario.bx[0] = Math.floor(mario.x[0]);
    mario.by[0] = Math.floor(mario.y[0]);
    if (mario.x[0] >= window.innerWidth / 2 / num1 && mario.x[0] <= map_x*16-window.innerWidth / 2 / num1) {
        map_coo.x = mario.x[0] - window.innerWidth/2/num1;
    }
    else if (mario.x[0] < window.innerWidth / 2 / num1) {
        map_coo.x = 0;
    }

    /*スコア表示**************************/
    states.score = Math.floor(states.coin / 100) * 10000 + states.point;

    document.getElementById("score").innerHTML =
        "time:" + ("00" + states.time).slice(-3) +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;score:" + ("000000" + states.score).slice(-7) +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;coin:" + ("000" + states.coin).slice(-4);

    /*************************************/
    if (mario.x[0] > states.goalline * 16) {
        states.game_clear();
    }
    /*************************************/

}
/******************************************/
function mario_drow() {
    var x2, y2;
    var x1, y1;
    window.scroll(map_coo.x*num1, 0);
    if (!mario.bottom) {
        mario.drowtype[0] = 4;
        cnt2 = 0;
    }
    else if (mario.speedx == 0) {
        mario.drowtype[0] = 0;
        cnt2 = 0;
    }
    else if ((mario.speedx >= 0 && mario.direction[0] == "left") || (mario.speedx <= 0 && mario.direction[0] == "right")) {
        mario.drowtype[0] = 5;
        cnt2 = 0;
    }
    else {
        if (cnt2 < 4) {
            mario.drowtype[0] = 1;
        }
        else if (cnt2 < 8) {
            mario.drowtype[0] = 2;
        }
        else if (cnt2 < 12) {
            mario.drowtype[0] = 3;
        }
        if (cnt2 >= 12) {
            cnt2 = 0;
        } else {
            cnt2 += 1;
        }
    }

    /*描画速度の計測*/
    fps.check();

    for (x1 = 0; x1 < mario.sizex; x1++) {
        for (y1 = 0; y1 < mario.sizey; y1++) {
            if (mario.img[mario.drowtype[1]][y1][x1] != 0) {
                y2 = mario.by[1] + y1;
                if (mario.direction[1] == "right") {
                    x2 = mario.bx[1] + x1;
                }
                else if (mario.direction[1] == "left") {
                    x2 = mario.bx[1] + mario.sizex + 3 - x1;
                }
                ctx.fillStyle = stgmake[0][y2][x2];
                ctx.fillRect(num1 * x2, num1 * y2, num1, num1);
            }
        }
    }

    for (x1 = 0; x1 < mario.sizex; x1++) {
        for (y1 = 0; y1 < mario.sizey; y1++) {
            if (mario.img[mario.drowtype[0]][y1][x1] != 0) {
                y2 = mario.by[0] + y1;
                if (mario.direction[0] == "right") {
                    x2 = mario.bx[0] + x1;
                }
                else if (mario.direction[0] == "left") {
                    x2 = mario.bx[0] + mario.sizex + 3 - x1;
                }
                ctx.fillStyle = mario.img[mario.drowtype[0]][y1][x1];
                ctx.fillRect(num1 * x2, num1 * y2, num1, num1);
            }
        }
    }
    mario.bx[1] = mario.bx[0];
    mario.by[1] = mario.by[0];
    mario.direction[1] = mario.direction[0];
    mario.drowtype[1] = mario.drowtype[0];

}
/******************************************/
function effect_byoga(y, x, cnt, type) {
    var img = new Array();
    if(type==3){
        if (cnt == 1) {
            img[00] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3];
            img[01] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            img[02] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            img[03] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            img[04] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            img[05] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            img[06] = [2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2];
            img[07] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
            img[08] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            img[09] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            img[10] = [2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1];
            img[11] = [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0];
            img[12] = [2, 2, 0, 1, 2, 0, 2, 2, 0, 2, 2, 1, 0, 2, 2, 0];
            img[13] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            img[14] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            img[15] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            for (var i = 0; i < 16; i++) {
                for (var j = 0; j < 16; j++) {
                    ctx.fillStyle = SCD(img[i][j]);
                    ctx.fillRect(num1 * (x +j), num1 *( y +i-16), num1 , num1);
                }
            }
            
        }
        if (cnt == 2) {
            ctx.fillStyle = "rgb(0,255,0)";
            ctx.fillRect(num1 * x * 16, num1 * y * 16, num1 * 16, num1 * 16);
        }
        if (cnt == 3) {
            ctx.fillStyle = "rgb(0,0,255)";
            ctx.fillRect(num1 * x * 16, num1 * y * 16, num1 * 16, num1 * 16);
        }
    }
}
/******************************************/
//よく使う色を簡単に指定するためのもの
function SCD(number) {
    if (number == 0) {
        return "rgb(107,140,255)";
    }
    if (number == 1) {
        return "rgb(0,0,0)";
    }
    if (number == 2) {
        return "rgb(231,90,16)";
    }
    if (number == 3) {
        return "rgb(247,214,181)";
    }
    if (number == 4) {
        return "rgb(255,165,66)";
    }
    if (number == 9) {
        return "rgb(255,0,0)";
    }
}
//念のためマリオ用
function MC(number) {
    if (number == 0) {
        return 0;
    }
    if (number == 1) {
        //防止の色
        return "rgb(0,0,255)";
    }
    if (number == 2) {
        //肌の色
        return "rgb(227,157,37)";
    }
    if (number == 3) {
        //服の色
        return "rgb(212,53,4)";
    }
    else {
        return 0;
    }
}
