﻿/* 1：白　2：黒 3:打てる白 4:打てる黒*/
var i, j, k;
//whiterotate,blackrotateで使用
var count2=0, F3, cell3 = new Array(), cell4 = new Array(), global_a, global_b;
var cell = new Array();
var cell2 = new Array();
for (i = -1; i < 9; i++) {
    cell[i] = new Array();
    cell2[i] = new Array();
    cell3[i] = new Array();
    cell4[i] = new Array();
}
var turn;
var f, F2;
//setIntervalでつかうかも
var count, timer,erasetimer;
var canvas, ctx;

window.onload = myupdate;

function myupdate() {
    document.getElementById("red_box").addEventListener("mousedown", onClick, false);
    //初期化
    for (i = -1; i < 9; i++) {
        for (j = -1; j < 9; j++) {
            cell[i][j] = 0;
            cell2[i][j] = false;
            cell3[i][j] = 0;
            cell4[i][j] = false;
        }
    }
    //セルの初期石位置
    cell[0] = [0, 0, 0, 0, 0, 0, 0, 0];
    cell[1] = [0, 0, 0, 0, 0, 0, 0, 0];
    cell[2] = [0, 0, 0, 0, 0, 0, 0, 0];
    cell[3] = [0, 0, 0, 1, 2, 0, 0, 0];
    cell[4] = [0, 0, 0, 2, 1, 0, 0, 0];
    cell[5] = [0, 0, 0, 0, 0, 0, 0, 0];
    cell[6] = [0, 0, 0, 0, 0, 0, 0, 0];
    cell[7] = [0, 0, 0, 0, 0, 0, 0, 0];
    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            cell3[i][j] = cell[i][j];
        }
    }
    //番
    if (turn == 1) {
        document.getElementById("textbox").innerHTML = "白の番です";
    }
    else {
        document.getElementById("textbox").innerHTML = "黒の番です";
    }

    canvas = document.getElementById("red_box");
    ctx = canvas.getContext("2d");
    canvas.width = 329;
    canvas.height = 329;
    
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillRect(0, 0, 1, 329);
    ctx.fillRect(41, 0, 1, 329);
    ctx.fillRect(82, 0, 1, 329);
    ctx.fillRect(123, 0, 1, 329);
    ctx.fillRect(164, 0, 1, 329);
    ctx.fillRect(205, 0, 1, 329);
    ctx.fillRect(246, 0, 1, 329);
    ctx.fillRect(287, 0, 1, 329);
    ctx.fillRect(328, 0, 1, 329);
    ctx.fillRect(0,0, 329,1);
    ctx.fillRect(0,41, 329,1);
    ctx.fillRect(0,82, 329,1);
    ctx.fillRect(0,123, 329,1);
    ctx.fillRect(0,164, 329,1);
    ctx.fillRect(0,205, 329,1);
    ctx.fillRect(0,246, 329,1);
    ctx.fillRect(0,287, 329,1);
    ctx.fillRect(0, 328, 329, 1);
    onDraw();

    document.getElementById("updatebutton").style.visibility = "hidden";
    document.getElementById("updatebutton").style.backgroundColor = "";
    document.getElementById("updatebutton").innerHTML = "";
    document.getElementById("log").innerHTML = "";
    document.getElementById("decision").innerHTML = "";

    F2 = false;
    turn = 2;
    mycheck(turn);
    KS(false);
}

function myEnter(num1, num2) {
    num1 = parseInt(num1);
    num2 = parseInt(num2);
    if (turn == 1) {
        f = false;
        for (i = -1; i < 2; i++) {
            for (j = -1; j < 2; j++) {
                if (cell[num1 + i][num2 + j] == 2 && !(i == 0 && j == 0)) {
                    k = 2;
                    while (true) {
                        if (cell[num1 + i * k][num2 + j * k] == 1) {
                            f = true;
                            while (k != 0) {
                                k--;
                                cell2[num1 + i * k][num2 + j * k] = true;
                            }
                            break;
                        }
                        else if (cell[num1 + i * k][num2 + j * k] == 2) {
                            k++;
                        }
                        else {
                            break;
                        }

                    }

                }
            }
        }

        if (cell[num1][num2] == 0 && f) {

            cell[num1][num2] = 1;
            for (i = 0; i < 8; i++) {
                for (j = 0; j < 8; j++) {

                    if (cell2[i][j]) {
                        cell[i][j] = 1;
                    }
                }
            }
            onDraw();
            if (mycheck(2)) {
                if (mycheck(1)) { KS(true) }
                else {
                    document.getElementById("decision").innerHTML = "パス";
                    erasetimer = setInterval("erasepass()", 1000);
                }
            }
            else {
                turn = 2;
                document.getElementById("textbox").style.color = "#ffff00";
                document.getElementById("textbox").innerHTML = "黒の番です";
            }
            document.getElementById("log").innerHTML = "<font color='#ffffff'>●</font>：" + num1 + "_" + num2 + "<br>" + document.getElementById("log").innerHTML;
            KS(false);
        }
        else {
            document.getElementById("textbox").style.color = "#ff0000";
            document.getElementById("textbox").innerHTML = "その場所には置けません";
        }
    }
    else if (turn == 2) {
        f = false;
        for (i = -1; i < 2; i++) {
            for (j = -1; j < 2; j++) {
                if (cell[num1 + i][num2 + j] == 1 && !(i == 0 && j == 0)) {
                    k = 2;
                    while (true) {
                        if (cell[num1 + i * k][num2 + j * k] == 2) {
                            f = true;
                            while (k != 0) {
                                k--;
                                cell2[num1 + i * k][num2 + j * k] = true;
                            }
                            break;
                        }
                        else if (cell[num1 + i * k][num2 + j * k] == 1) {
                            k++;
                        }
                        else {
                            break;
                        }

                    }

                }
            }
        }


        if (cell[num1][num2] == 0 && f) {
            cell[num1][num2] = 2;
            for (i = 0; i < 8; i++) {
                for (j = 0; j < 8; j++) {

                    if (cell2[i][j]) {
                        cell[i][j] = 2;
                    }

                }
            }
            onDraw();
            if (mycheck(1)) {
                if (mycheck(2)) { KS(true) }
                else {
                    document.getElementById("decision").innerHTML = "パス";
                    erasetimer = setInterval("erasepass()", 1000);
                }
            }
            else {
                turn = 1;
                document.getElementById("textbox").style.color = "#ffff00";
                document.getElementById("textbox").innerHTML = "白の番です";
            }
            document.getElementById("log").innerHTML = "<font color='#000000'>●</font>：" + num1 + "_" + num2 + "<br>" + document.getElementById("log").innerHTML;
            KS(false);
        }
        else {
            document.getElementById("textbox").style.color = "#ff0000";
            document.getElementById("textbox").innerHTML = "その場所には置けません";
        }
    }

    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            cell3[i][j] = cell[i][j];
        }
    }
    myReset_cell(cell2);
    myrotate();
}
//最後
function myReset_cell(cell) {
    var a, b;
    for (a = -1; a < 9; a++) {
        for (b = -1; b < 9; b++) {
            cell[a][b] = false;
        }
    }
}
//次打てる場所を黄緑色にしたり詰んだ時の判定をしたり
//次置ける場所がなかった場合、返り値はtrue
function mycheck(myturn) {
    var enemyturn, num1, num2;
    var ローカル詰んだ判定 = true;
    if (myturn == 1) {
        enemyturn = 2;
    }
    else if (myturn == 2) {
        enemyturn = 1;
    }

    for (num1 = 0; num1 < 8; num1++) {
        for (num2 = 0; num2 < 8; num2++) {
            if (cell[num1][num2] == 0) {
                for (i = -1; i < 2; i++) {
                    for (j = -1; j < 2; j++) {
                        if ((cell[num1 + i][num2 + j] == enemyturn) && (!(i == 0 && j == 0))) {
                            k = 2;
                            while (true) {
                                if (cell[num1 + i * k][num2 + j * k] == myturn) {
                                    ctx.fillStyle = "rgb(127,255,0)";
                                    ctx.fillRect(num2 * 41 + 1, num1 * 41 + 1, 40, 40);
                                    ローカル詰んだ判定 = false;
                                    break;
                                }
                                else if (cell[num1 + i * k][num2 + j * k] == enemyturn) {
                                    k++;
                                }
                                else {
                                    break;
                                }

                            }

                        }
                    }
                }
            }

        }
    }
    return ローカル詰んだ判定;
}
//tableに色つけたり
function onDraw() {
    var a, b;
    for (a = 0; a < 8; a++) {
        for (b = 0; b < 8; b++) {
            if (cell[a][b] == 0) {
                ctx.fillStyle = "rgb(0,100,0)";
                ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
            }
            else if (cell[a][b] == 1) {
                if (cell3[a][b]==2) {
                    cell4[a][b] = true;
                }
                else {
                    ctx.fillStyle = "rgb(0,100,0)";
                    ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = "rgb(0,0,0)";
                    ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                    ctx.fill();
                    ctx.restore();
                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = "rgb(255,255,255)";
                    ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                    ctx.fill();
                    ctx.restore();
                }
            }
            else if (cell[a][b] == 2) {
                if (cell3[a][b] == 1) {
                    cell4[a][b] = true;
                }
                else {
                    ctx.fillStyle = "rgb(0,100,0)";
                    ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = "rgb(255,255,255)";
                    ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                    ctx.fill();
                    ctx.restore();
                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = "rgb(0,0,0)";
                    ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                    ctx.fill();
                    ctx.restore();
                }
            }
        }
    }
}
//黒と白の色を数えて勝敗判定をしたり
//Endsがtrueになったら試合終了
function KS(Ends) {
    var black = 0, white = 0;
    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            if (cell[i][j] == 1) {
                white++;
            }
            else if (cell[i][j] == 2) {
                black++;
            }
        }
    }
    if (white + black == 64 || white == 0 || black == 0 || Ends) {
        F2 = true;
        if (white > black) {
            document.getElementById("decision").innerHTML = "白の勝ち";
        }
        else if (white < black) {
            document.getElementById("decision").innerHTML = "黒の勝ち";
        }
        else {
            document.getElementById("decision").innerHTML = "引き分け";
        }
        clearInterval(timer);
        document.getElementById("updatebutton").style.visibility = "visible";
        document.getElementById("updatebutton").style.backgroundColor = "#000000";
        document.getElementById("updatebutton").innerHTML = "初めに戻す";
    }
    document.getElementById("kurosiro").innerHTML =
        "●…" + black + "<br>○…" + white;
}
//「パス」という文字を消すためだけに存在する
function erasepass() {
    if (!F2) {
        document.getElementById("decision").innerHTML = "";
        clearInterval(erasetimer);
    }
}

function autostart() {
    timer = setInterval("automatic()", 1000 / 64);
}

function automatic() {
    if (turn == 1) {
        enemyturn = 2;
    }
    else if (turn == 2) {
        enemyturn = 1;
    }
    var a = new Array();
    var count = 0;
    for (var i = 0; i < 8; i++) {
        a[i] = new Array()
        for (var j = 0; j < 8; j++) {
            a[i][j] = 0;
        }
    }

    var num1, num2;
    for (num1 = 0; num1 < 8; num1++) {
        for (num2 = 0; num2 < 8; num2++) {
            if (cell[num1][num2] == 0) {
                for (i = -1; i < 2; i++) {
                    for (j = -1; j < 2; j++) {
                        if ((cell[num1 + i][num2 + j] == enemyturn) && (!(i == 0 && j == 0))) {
                            k = 2;
                            while (true) {
                                if (cell[num1 + i * k][num2 + j * k] == turn) {
                                    if (a[num1][num2]) {
                                    } else {
                                        a[num1][num2] = 1;
                                        count++;
                                    }
                                    break;
                                }
                                else if (cell[num1 + i * k][num2 + j * k] == enemyturn) {
                                    k++;
                                }
                                else {
                                    break;
                                }

                            }

                        }
                    }
                }
            }

        }
    }
    if (count == 0) {
        return false;
    }
    count = Math.floor(Math.random() * (count - 1));
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            if (a[i][j] == 1) {
                if (count == 0) {
                    myEnter(i, j);
                    return false;
                } else {
                    count--;
                }
            }
        }
    }
    return false;

}

function onClick(e){
    var num1, num2;
    for (i = 0; i < 8; i++) {
        if (i * 41 < (e.offsetY) && (e.offsetY) < (i + 1) * 41) {
            num1 = i;
        }
        if (i * 41 < (e.offsetX) && (e.offsetX) < (i + 1) * 41) {
            num2 = i;
        }
    }
    myEnter(num1, num2);
}

function myrotate() {
    var a, b;
    for (a = 0; a < 8; a++) {
        for (b = 0; b < 8; b++) {
            if (cell4[a][b]) {
                if (cell[a][b]==2) {
                    if (count2 == 0) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 1) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.translate(b * 41 + 24, a * 41 + 23);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.translate(b * 41 + 20, a * 41 + 20);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 2) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.translate(b * 41 + 18, a * 41 + 19);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.translate(b * 41 + 22, a * 41 + 22);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 3) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                }
                else if (cell[a][b] == 1) {
                    if (count2 == 0) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 1) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.translate(b * 41 + 24, a * 41 + 23);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.translate(b * 41 + 20, a * 41 + 20);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 2) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.translate(b * 41 + 18, a * 41 + 19);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.translate(b * 41 + 22, a * 41 + 22);
                        ctx.rotate(Math.PI / 6)
                        ctx.scale(0.8, 1.2);
                        ctx.arc(0, 0, 13, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                    if (count2 == 3) {
                        ctx.fillStyle = "rgb(0,100,0)";
                        ctx.fillRect(b * 41 + 1, a * 41 + 1, 40, 40);
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(0,0,0)";
                        ctx.arc(b * 41 + 22, a * 41 + 23, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                        ctx.save();
                        ctx.beginPath();
                        ctx.fillStyle = "rgb(255,255,255)";
                        ctx.arc(b * 41 + 21, a * 41 + 21, 15, 0, Math.PI * 2, false);
                        ctx.fill();
                        ctx.restore();
                    }
                }
            }
        }
    }
    count2++;
    if (count2 == 4) {
        count2 = 0;
        myReset_cell(cell4);
        return false;
    }
    if (count2 != 4) {
        setTimeout("myrotate()", 100);
    }
}

